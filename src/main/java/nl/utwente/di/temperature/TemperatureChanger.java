package nl.utwente.di.temperature;

public class TemperatureChanger {
    /**
     * Returns the price for a given isbn
     * @param isbn the isbn of the book to check the price for
     * @return the price of the isbn
     */
    public float getTemperatureCelsius(String temperature) {
        return (Float.parseFloat(temperature) -32 )/1.8f;
    }
}
