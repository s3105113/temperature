package nl.utwente.di.temperature;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/** Tests the quoter */
public class TestQuoter {
    @Test
    public void testBook1() throws Exception {
        TemperatureChanger quoter = new TemperatureChanger();
        double price = quoter.getTemperatureCelsius("23");
        assertEquals(-5.0,price,0.0);
    }
}
